/* Employee Leave Management System 

Author: Lee Kah Kin
Asia Pacific University
UC1F1705CS(DA), ICP
Compiled on Windows 10-64bit with gcc (MinGW.org GCC-6.3.0-1) 6.3.0
I've added some code here
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

struct User_Profile {
	char firstname[50];
	char lastname[50];
	char faculty[120];
	char age [10];
};

struct Leave {
	int leave_ID;
	char USERID[50];
	char Reason[280];
	char status[50];
	int month;
	int day;
	int noLeaves;
	struct Leave *next;
};
void loginFile();
int elmsLogin(char* user, char* password);
void createProfile();
void profileDetails(int validator);
void openProfile(char* profile_search);
void openUserList();
void delete_profile_login(char* mod_profile);
void delete_profile_userList(char* mod_profile);
void delete_profile(char* mod_profile);
void modify_profile(char* mod_file);
void faq();
void public_holiday();
void edit_public_holiday();
int leave_counter (); 
int append_leave(struct Leave** head_ref, char* USERID,int l_count);
void view_status(char* user);
void view_application();
void approve_application();

int main()
{
	char user[100];
	char password[100]; 
	char profile_search[25],mod_profile[25], mod_file[25];
	int menu_options, admin_option, main_menu = 1,search_option,mod_option,AL_option,lec_option,leave_menu;
	int l_count =0;
	struct Leave* head = NULL;
	time_t today;	
	time(&today);

	if (access ("login.txt", F_OK) != 0) {
		loginFile(); //creates login file if it does not exist
	}
	l_count =leave_counter() +1; //checks for leave_id, ensuring it is always latest one
	printf("Welcome to ELMS!\n");
	printf("Today's date and time: %s\n", ctime(&today));
	while (1) {
		
		int menu_check = -1;
		printf("CHOOSE YOUR OPTIONS:\n(1) LOGIN \n(2) EXIT\nOPTIONS: ");
		scanf(" %d", &menu_options);
		if (menu_options == 1){
			
			while (menu_check == -1){
				
				printf("\n----userID 99 is for exit----\n");
				printf("Login(USERID):  ");
				scanf("%s", user);
				printf("\nPassword:  ");
				scanf(" %s", password);
				menu_check = elmsLogin(user,password);
				
				if (menu_check == 1) {
					
					system("cls");
					printf("===============LOGIN SUCCESSFUL!===============\n");
					printf("***************WELCOME ADMIN***************\n\n\n\n\n");
					while (1) {
						
						printf("Choose options:\n(1) CREATE NEW PROFILE\n(2) Search userlist\n(3) Modify Profile\n(4) View Employee Leave Status\n(5) Edit Public Holidays\n(9)RETURN TO MAIN MENU\nYour choice:");
						scanf(" %d", &admin_option);
						if (admin_option == 1) {
							
							system("cls");
							printf("---------------PROFILE CREATION---------------\n");
							printf("CREATE LOGIN ID, PASSWORD, AND PROFILE TYPE\n");
							createProfile();
						}
						else if (admin_option == 2) {
							
							system("cls");
							printf("----------SEARCH----------\n(1)Enter user_ID to check profile\n(2)Search userlist to find profile\nENTER OPTION: ");
							scanf(" %d", &search_option);
							if (search_option == 1) {
								
								system("cls");
								printf("Please enter USERID\n");
								scanf(" %s", profile_search);
								system("cls");
								openProfile(profile_search);							
						}
							else if (search_option == 2) {
								system("cls");
								openUserList();
							}
						}
						else if (admin_option == 3) {
							
							system("cls");
							printf("**********MODIFICATIONS**********\n");
							printf("(1)DELETE PROFILE (WARNING: CHANGES CANNOT BE UNDONE)\n(2)Modify Profile\nENTER OPTION: ");
							scanf(" %d", &mod_option);
							
							if (mod_option == 1) {
								
								system("cls");
								printf("WARNING, ALL CHANGES ARE PERMANENT, PROCEED WITH CAUTION");
								printf("\nPlease enter profile_ID to be deleted:");
								scanf(" %s", mod_profile);
								delete_profile_login(mod_profile);
								delete_profile_userList(mod_profile);
								delete_profile(mod_profile);
								printf("All changes have been committed.\n");
								system("pause");
								system("cls");
								}
								
							else if(mod_option == 2) {
								
								system("cls");
								printf("\nPlease select userID to modify: ");
								scanf(" %s", mod_file);
								modify_profile(mod_file);
								system("pause");
								system("cls");
								}
						}
						else if (admin_option == 4) {
							view_application();
							system("pause");
							system("cls");
						}
						else if (admin_option == 5) {
							edit_public_holiday();
							system("pause");
							system("cls");
						}
						else if (admin_option == 9){
							break;
						}
						else {
							printf("Invalid Option, please try again\n");
							system("pause");
							system("cls");
						}//admin_menu end else
					} //end WHILE (admin_menu) 	
				}//end IF (menu_check == 1) 
				else if(menu_check == 2) {
					system("cls");
					printf("===============LOGIN SUCCESSFUL!===============\n");
					printf("***************WELCOME ACADEMIC LEADER***************\n\n\n\n\n");
					while(1) {
						printf("Choose options:\n(1) View Leave Application\n(2) Public Holidays\n(3) FAQ\n(4) Approve Leave Application\n\n(9)RETURN TO MAIN MENU\nYour choice:");
						scanf(" %d", &AL_option);
						if (AL_option == 1) {
							
							view_application();
							system("pause");
							system("cls");
						}
						else if (AL_option ==2) {
							
							public_holiday();
							system("pause");
							system("cls");							
						}
						else if (AL_option == 3) {
							faq();
							system("pause");
							system("cls");
						}
						else if (AL_option == 4) {
							approve_application();
							system("pause");
							system("cls");
						}
						
						else if (AL_option == 9) {
							break;
						}
						else {							
							printf("Invalid Option, please try again\n");
							system("pause");
							system("cls");
						}
						
					}
				}
				
				else if (menu_check == 3) {
					system("cls");
					printf("===============LOGIN SUCCESSFUL!===============\n");
					printf("***************WELCOME LECTURER***************\n\n\n\n\n");
					
					while(1) {
						printf("Choose options:\n(1) Apply for Leave\n(2) Public Holidays\n(3) FAQ\n\n(9)RETURN TO MAIN MENU\nYour choice:");
						scanf(" %d", &lec_option);
						if (lec_option == 1) {
							system("cls");
							printf("(1)Leave Application\n(2)View Application status\n");
							scanf(" %d", &leave_menu);
							if (leave_menu == 1) {
								
								if(append_leave(&head, user, l_count) == 0)
									l_count =leave_counter();
									l_count += 1;
									system("pause");
									system("cls");									
							}else if (leave_menu == 2) {
								
								view_status(user);
								system("pause");
								system("cls");								
							}
						}
						else if (lec_option == 2) {
							public_holiday();
							system("pause");
							system("cls");
						}
						else if (lec_option == 3) {
							faq();
						} //end IF
						
						else if (lec_option == 9) {
							break;
						} //end IF
						
						else {
							printf("Invalid Option, please try again\n");
							system("pause");
							system("cls");
						} //end ELSE						
					} //end WHILE 					
				}//end IF (menu_check) 
				else if (menu_check == 4) {
					break;
				}//end IF (menu_check)
			} //end while (menu_check)
		}	//end if (menu_options)	
		else {
			break;
		}//end else
	}//end main_menu while
	
	printf("Goodbye!\n");
	system("pause");
    return 0;
} //end main

void loginFile() //creates a new login file with admin if it does not exist
{
	FILE *newlogin;
	
	newlogin = fopen("login.txt", "w");
	fprintf(newlogin,"%s %s %d\n","admin","admin",1);
	fclose(newlogin);
}

int elmsLogin (char* user, char* password)
{
	int login_ret = -1; //defines login success/failure and user privellege
	FILE *login;
	char user_check[100], password_check[100];
	
	login = fopen("login.txt","r");	
	if (login == NULL){
		perror("File not found\n");
		return -1;
	}
	if (strcmp(user, "99") == 0)
		return 4;
	
	while (!feof(login)){
		fscanf(login, "%s%s%", user_check, password_check);
		//printf("input:%s\n in_check:%s\n in:%s\n in_check:%s\n",user,user_check,password,password_check); --for debugging
		if (strcmp(user, user_check) == 0 && strcmp(password,password_check) == 0) //compares password
			fscanf(login,"%d\n",&login_ret); 
		fscanf(login,"%*d\n"); //if no match, scanf needed to read the extra digit to prevent input errors
	}
	if (login_ret == -1)
		printf("Username or password invalid, please try again!\n");
	
	fclose(login);
	
	return login_ret;
}

void createProfile() //creating new profile file for user
{
	FILE *profile, *staffInfo, *searchfile;
	char profile_check[100], profile_ID[100], profile_password[100];
	char filename[130];
	int profile_type=0, validator=0;
	struct User_Profile createNewUser;
	
	printf("Enter ID: \n");	
	profile = fopen("login.txt", "r");
	scanf(" %s", profile_ID);

	while (fscanf(profile, "%s\n", profile_check) != EOF) {	//checks for ID duplicate	
		if (strcmp(profile_ID, profile_check) == 0) {
			printf("ID already in use!\n\n\n\n");
			return;
		}
	}
	
	printf("\nPassword: ");	
	scanf(" %s", profile_password);
	printf("\nProfile Type (2 for Academic leader, 3 for Lecturer): "); //for login_ret 
	while (1){
		scanf(" %d", &profile_type);
		if (profile_type != 2 && profile_type !=3) {
			printf("Please only enter 2 or 3\n");
		}
		else {
			break;
		}
	}
	
	printf("\n%s %s %d\n", profile_ID, profile_password, profile_type);	
	
	fclose(profile);
	profile = fopen("login.txt", "a");
	fprintf(profile,"%s %s %d\n", profile_ID, profile_password, profile_type);
	fclose(profile);
	//creating profile file
	
	system("cls");
	printf("Login created, please create a profile for the staff");
	printf("\n=====================================================");
	fflush(stdin);
	printf("\nFIRST NAME: ");
	scanf(" %s", createNewUser.firstname);
	printf("\nLAST NAME: ");
	scanf(" %s", createNewUser.lastname);
	printf("\nAGE: ");
	scanf(" %s",createNewUser.age);
	printf("\nFaculty(IT,BM,ENG):  ");
	scanf(" %s",createNewUser.faculty);
	strcpy(filename, profile_ID);
	strcat(filename,".txt");
	
	staffInfo = fopen(filename,"w");
	fprintf(staffInfo,"%s %s %s %s %s", profile_ID, createNewUser.firstname, createNewUser.lastname, createNewUser.faculty, createNewUser.age);
	fclose(staffInfo);
	
	//userlist for searching name and ID
	searchfile = fopen("userlist.txt", "a");
	fprintf(searchfile,"%s %s\n", profile_ID,strcat(createNewUser.firstname,createNewUser.lastname));
	fclose(searchfile);
	
	printf("Login and profile has been created\n");
	system("pause");
	system("cls");
	return;
}

void openProfile(char* profile_search) //display user profile
{
	FILE *openProfile;
	char userID[100],firstname[50],lastname[50],faculty[50],age[10];
	
	openProfile = fopen(strcat(profile_search,".txt"), "r");	
	if (openProfile == NULL) {
		perror("\nUser_ID does not exist, please try another!\n");
		return;
	}
	while (!feof(openProfile))
	{
		fscanf(openProfile,"%s%s%s%s%s",userID,firstname,lastname,faculty,age);
		printf("USERID: %s\n", userID);
		printf("FIRSTNAME: %s\n", firstname);
		printf("LASTNAME: %s\n", lastname);
		printf("FACULTY: %s\n", faculty);
		printf("AGE %s\n", age);
	}
	system("pause");
	system("cls");
	return;
}
void openUserList() //ID:Name search 
{
	FILE *openUserList;
	char ID[50],name[230];
	
	printf("---USER DIRECTORY---\nID   Name\n");
	openUserList = fopen("userlist.txt", "r");
	if (openUserList == NULL) {
		perror("\nFile does not exist, please try another!\n");
		return;
	}	
	while (!feof(openUserList)){
		fscanf(openUserList,"%s%s\n",ID,name);
		printf("%s  %s\n", ID,name);
	}
	fclose(openUserList);
	system("pause");
	system("cls");
	return;
}
void delete_profile_login(char* mod_profile)
{
	FILE *ori_ptr, *cp_ptr;
	char login[100],password[100], profile_type[3];
	
	ori_ptr = fopen("login.txt", "r");
	if (ori_ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}
	while(!feof(ori_ptr)){
		fscanf(ori_ptr,"%s%s%s\n", login,password,profile_type);
		//printf("%s %s %s\n",login,password,profile_type); --for debugging
	}
	
	rewind(ori_ptr); //moves pointer back to the start
	cp_ptr = fopen("login.tmp", "w");
	
	while(!feof(ori_ptr)) {
		fscanf(ori_ptr,"%s%s%s\n", login,password,profile_type);
		if (strcmp(login, mod_profile) != 0) {//prints to a copy file ONLY IF there is no match with userID
			fprintf(cp_ptr,"%s %s %s\n",login,password,profile_type);
		}
	}
	fclose(ori_ptr);
	fclose(cp_ptr);
	if(remove("login.txt") == 0) {
		rename("login.tmp", "login.txt");
		printf("\nLogin information has been removed from the file.");
	}else 
		perror("ABORTING! CONTACT YOUR ADMINSTRATOR FOR HELP\n");	
	return;
}

void delete_profile_userList(char* mod_profile)
{
	FILE *ori_ptr, *cp_ptr;
	char user_ID[100], name[100];
	
	ori_ptr = fopen("userlist.txt", "r");
	if (ori_ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}
	
	while(!feof(ori_ptr)) {
		fscanf(ori_ptr,"%s%s\n", user_ID,name);
	}
	rewind(ori_ptr);
	
	cp_ptr = fopen("userlist.tmp", "w");
	while(!feof(ori_ptr)) {
		fscanf(ori_ptr,"%s%s\n", user_ID,name);
		if (strcmp(user_ID, mod_profile) != 0) {
			fprintf(cp_ptr,"%s %s\n", user_ID, name); //prints to a copy file ONLY IF there is no match with userID
		}
	}
	fclose(ori_ptr);
	fclose(cp_ptr);
	if(remove("userlist.txt") == 0) {
		rename("userlist.tmp", "userlist.txt");
		printf("\nUser info has been removed from query table.\n");
	}else
		perror("ABORTING! CONTACT YOUR ADMINSTRATOR FOR HELP\n");
	return;
}		

void delete_profile(char* mod_profile) 
{
	int check;
	strcat(mod_profile,".txt");
	if(remove(mod_profile) == 0) {
		printf("File removed!\n");
	}
	else {
		perror("File cannot be deleted!\n");
	}
	return;
}

void modify_profile(char* mod_file)
{
	FILE *ori_ptr, *cp_ptr;
	char user_ID[100],firstname[50],lastname[50],faculty[100],age[10];
	int choice;
	
	strcat(mod_file,".txt");
	ori_ptr = fopen(mod_file,"r");
	if (ori_ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}
	
	while (!feof(ori_ptr)) {
		fscanf(ori_ptr,"%s%s%s%s%s\n",user_ID,firstname,lastname,faculty,age);
	}
	printf("(1)Change Age\n(2)Change Faculty\n(3)Change First name\n(4)Change Last name\nChoose Option:");
	scanf(" %d", &choice);
	if (choice == 1) {
		printf("Change Age to:  \n");
		scanf(" %s", age);
	}
	else if (choice == 2) {
		printf("Change Faculty to: \n");
		scanf(" %s", faculty);
	}
	else if (choice == 3) {
		printf("New First Name? : \n");
		scanf(" %s", firstname);
	}
	else if (choice == 4) {
		printf("New First Name? : \n");
		scanf(" %s", lastname);
	}
	rewind(ori_ptr);
	cp_ptr = fopen("temp.txt", "w");
	fprintf(cp_ptr,"%s %s %s %s %s",user_ID,firstname,lastname,faculty,age);
	fclose(ori_ptr);
	fclose(cp_ptr);
	if(remove(mod_file) == 0){	
		rename("temp.txt", mod_file);
	}else
		perror("ABORTING! CONTACT YOUR ADMINSTRATOR FOR HELP\n");
	return;
}

void faq()
{
	int choice;
	system("cls");
	while(1) {
		printf("---------------FREQUENTLY ASKED QUESTIONS---------------\n");
		printf("---------------UNIVERSITY LEAVE POLICY------------------\n");
		printf("(1)How many days of leave do I get?\n");
		printf("(2)Is it possible to get half day leave?\n");
		printf("(3)Does sick days counts as leave?\n");
		printf("(4)Do i have to take leave on Public Holidays?\n");
		printf("(5)How do I apply for leave?\n");
		printf("(6)What happens to leftover leave(s)?\n");
		printf("(7)What if my application was rejected?\n");
		printf("(8)I can't find my leave status?\n");
		
		printf("(9)EXIT\n");
		
		scanf(" %d", &choice);
		switch(choice) {
			case 1:
				printf("You get 20 days of leave every year\n");
				system("pause");
				system("cls");
				break;			
			case 2:
				printf("No. However, you may call in sick if you do not feel well.\n");
				system("pause");
				system("cls");		
				break;
			case 3:
				printf("Sick days are not considered leave taken out.\n");
				system("pause");
				system("cls");
				break;
			case 4:
				printf("You do not have to.\n");
				system("pause");
				system("cls");
				break;
			case 5:
				printf("You may apply through ELMS, simply fill in the forms. Your academic leader will process it soon.\n");
				system("pause");
				system("cls");
				break;
			case 6:
				printf("All leftover leave(s) are considered BURNED. It will not be carried over.\n");
				system("pause");
				system("cls");
				break;
			case 7:
				printf("Please go and meet you academic leader.\n");
				system("pause");
				system("cls");
				break;
			case 8:
				printf("You need to apply for a leave first to be able to see\n");
				system("pause");
				system("cls");
			case 9:
				printf("Exiting.....\n");
				system("pause");
				system("cls");
				return;
			default:
				printf("Invalid choice\n");
				system("pause");
				system("cls");
		}
	}	
}
void public_holiday()
{
	FILE *ptr;
	char ch;
	system("cls");
	ptr = fopen("holiday.txt", "r");
	ch = getc(ptr);
	while(ch != EOF) {
		printf("%c", ch);
		ch = getc(ptr);
	}
	fclose(ptr);
	system("pause");
	system("cls");
}

void edit_public_holiday()
{
	system("cls");
	system("notepad holiday.txt");
}

int leave_counter ()
{
	FILE *ptr;
	char buf[600],ch,USERID[30],Reason[240],status[60];
	int leave_ID,month,day,noLeaves;
	
	ptr = fopen("leave.txt", "r");
	
	if (ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return 0;
	}	

	while(!feof(ptr)) {
		fgets(buf, 599, ptr);
		sscanf(buf, "%d%s%[^,],%d,%d,%s%d", &leave_ID,USERID,Reason,&month,&day,status,&noLeaves);
		//fscanf(ptr, "%d,%d,%s,%s,%d,%d,%s,%d\n", leave.leave_ID,leave.USERID,leave.Reason,leave.month,leave.day,leave.status,leave.noLeaves);
	}

	fclose(ptr);
	
	return leave_ID;
}

	
int append_leave (struct Leave** head_ref, char* USERID, int l_count)
{
	struct Leave leave;
	struct Leave* new_node = (struct Leave*) malloc(sizeof(struct Leave));
	struct Leave *last = *head_ref;
	FILE *ptr;
	ptr = fopen("leave.txt", "a");
	
	printf("LEAVE APPLICATION FORM\n");
	printf("Please key in your reason: \n");
	fflush(stdin);
	fgets(leave.Reason,279,stdin);
	while(1) {
		
		printf("Enter the month of leave: ");
		scanf(" %d", &leave.month);
		
		if (leave.month <= 0 || leave.month > 12)
			printf("Invalid date, please try again!\n");
		else
			break;
	}
	while(1) {		
		printf("Enter the day of leave: ");
		scanf(" %d", &leave.day);
		
		if (leave.day <= 0 || leave.day > 31)
			printf("Invalid date, please try again!\n");
		else
			break;
	}
	
	strcpy(new_node->Reason, strtok(leave.Reason,"\n"));	
	new_node->leave_ID = l_count;
	strcpy(new_node->USERID,USERID);
	new_node->month = leave.month;
	new_node->day = leave.day;
	strcpy(new_node->status, "Pending");
	new_node->noLeaves = 20;
	
	new_node->next = NULL;
	if (*head_ref == NULL) {
		
		*head_ref = new_node;
		fprintf(ptr,"%d,%s, %s,%d,%d,%s,%d\n",new_node->leave_ID,new_node->USERID,new_node->Reason,new_node->month,new_node->day,new_node->status,new_node->noLeaves);
		fclose(ptr);
		return 1;
	}
	while(last->next!=NULL)
		last = last->next;
	
	
	fprintf(ptr,"%d,%s, %s,%d,%d,%s,%d\n",new_node->leave_ID,new_node->USERID,new_node->Reason,new_node->month,new_node->day,new_node->status,new_node->noLeaves);
	fclose(ptr);

	last->next = new_node;
	
	return 0;
}

void view_status (char* user)
{
	FILE *ptr;
	char ch,USERID[30],Reason[240],status[60];
	int leave_ID,month,day,noLeaves;
	
	ptr = fopen("leave.txt", "r");
	
	if (ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}

	printf("LEAVE ID  : STATUS\n");
	
	while(!feof(ptr)) {
		fscanf(ptr,"%d%s %[^,],%d,%d,%[^,],%d\n", &leave_ID,USERID,Reason,&month,&day,status,&noLeaves);		
		
		if (strcmp(strtok(USERID,","), user) == 0) {
			
			printf("%d        : %s\n\n",leave_ID,status);			
		}
	}

	fclose(ptr);
	
	return;
}

void view_application ()
{
	FILE *ptr;
	char ch,USERID[30],Reason[240],status[60];
	int leave_ID,month,day,noLeaves;
	
	ptr = fopen("leave.txt", "r");
	
	if (ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}
	
	while(!feof(ptr)) {

		fscanf(ptr,"%d%s %[^,],%d,%d,%[^,],%d\n", &leave_ID,USERID,Reason,&month,&day,status,&noLeaves);
		printf("==============================================\n");
		printf("LEAVE_ID:  %d\n", leave_ID);
		printf("USERID:    %s\n", strtok(USERID,","));
		printf("REASON:    %s\n", Reason);
		printf("MONTH:     %d\n", month);
		printf("DAY:       %d\n", day);
		printf("STATUS:    %s\n", status);
		printf("==============================================\n");
		printf("LEAVES REMAINING: %d\n\n\n", noLeaves);
	}

	fclose(ptr);
	
	return;
}

void approve_application()
{
	FILE *ptr, *ptr2, *ptr3;
	char ch,USERID[30],Reason[240],status[60];
	int leave_ID,month,day,noLeaves,check_ID,leave_app,sick_check;
	
	ptr = fopen("leave.txt", "r");

	if (ptr == NULL) {
		perror("\nUser does not exist, please try another");
		return;
	}
	system("cls");
	printf("*****CHOOSE LEAVE ID*****\n");
	scanf(" %d", &check_ID);
	while(!feof(ptr)) {

		fscanf(ptr,"%d%s %[^,],%d,%d,%[^,],%d\n", &leave_ID,USERID,Reason,&month,&day,status,&noLeaves);

		if (check_ID == leave_ID) {
			printf("==============================================\n");
			printf("LEAVE_ID:  %d\n", leave_ID);
			printf("USERID:    %s\n", strtok(USERID,","));
			printf("REASON:    %s\n", Reason);
			printf("MONTH:     %d\n", month);
			printf("DAY:       %d\n", day);
			printf("STATUS:    %s\n", status);
			printf("==============================================\n");
			printf("LEAVES REMAINING: %d\n\n\n", noLeaves);
			printf("(1)Approve or (2)Reject (3)EXIT: \n");
			
			scanf(" %d", &leave_app);
			if (leave_app == 1) {
				strcpy(status, "APPROVED!");
				printf("Is it sick leave?\n(1)Yes\n(2)No\n");
				scanf(" %d", &sick_check);

				if (sick_check == 2) {
					noLeaves -= 1;
					break;//if not sick, remaining leaves decreases
				}else {

					break;
				}
				
			}else if (leave_app == 2) {
				strcpy(status, "REJECTED!\n");
				break;
			}
			else {
				printf("Exiting.....\n");
			}			
		}
	}
	rewind(ptr);
	ptr2 = fopen("leave.tmp", "w");
	fprintf(ptr2,"%d%s, %s,%d,%d,%s,%d\n", leave_ID,USERID,Reason,month,day,status,noLeaves);
	fclose(ptr2);
	
	ptr3 = fopen("leave.tmp", "a");
	while(!feof(ptr)) {
		
		fscanf(ptr,"%d%s %[^,],%d,%d,%[^,],%d\n", &leave_ID,USERID,Reason,&month,&day,status,&noLeaves);
		if (check_ID != leave_ID) {
			fprintf(ptr3,"%d%s, %s,%d,%d,%s,%d\n", leave_ID,USERID,Reason,month,day,status,noLeaves);
		}
	}	
	fclose(ptr);
	fclose(ptr3);
	remove("leave.txt");
	rename("leave.tmp", "leave.txt");
	return;
}